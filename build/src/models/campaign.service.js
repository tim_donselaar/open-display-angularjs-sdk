(function() {
    'use strict';

    campaignFactory.$inject = ["odApi"];
    angular
        .module('od.api')
        .factory('odCampaign', campaignFactory);
    
    /**
     * @ngdoc service 
     * @name od.api.odCampaign
     * @description
     *
     * The `odCampaign` service provides a convenient wrapper for campaign related requests.
     *
     */

    /* @ngInject */
    function campaignFactory(odApi) {

        var service = {
            copy: copy,
            create: create,
            list: list,
            remove: remove,
            clearResults: clearResults,
            update: update
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name copy
         * @methodOf od.api.odCampaign
         *
         * @description
         * 
         *
         * @param {Object} data data options object.
         */
        function copy(data) {
            return odApi.request('/2/Campaigns/Copy', data);
        }

        /**
         * @ngdoc method
         * @name create
         * @methodOf od.api.odCampaign
         *
         * @description
         * 
         *
         * @param {Object} data data options object.
         */
        function create(data) {
            return odApi.request('/2/Campaigns/New', data);
        }

        /**
         * @ngdoc method
         * @name list
         * @methodOf od.api.odCampaign
         *
         * @description
         * 
         *
         * @param {Object} data data options object.
         */
        function list(data) {
            return odApi.request('/2/Campaigns/List', data);
        }

        /**
         * @ngdoc method
         * @name remove
         * @methodOf od.api.odCampaign
         *
         * @description
         * 
         *
         * @param {Object} data data options object.
         */
        function remove(data) {
            return odApi.request('/2/Campaigns/Remove', data);
        }

         /**
         * @ngdoc method
         * @name clearResults
         * @methodOf od.api.odCampaign
         *
         * @description
         * 
         *
         * @param {Object} data data options object.
         */
        function clearResults(data) {
            return odApi.request('/2/Campaigns/ClearResults', data);
        }

        /**
         * @ngdoc method
         * @name update
         * @methodOf od.api.odCampaign
         *
         * @description
         * 
         *
         * @param {Object} data data options object.
         */
        function update(data) {
            return odApi.request('/2/Campaigns/Update', data);
        }
    }
})();
