(function () {
    'use strict';

    playlistFactory.$inject = ["odApi"];
    angular
            .module('od.api')
            .factory('odPlaylist', playlistFactory);

    function playlistFactory(odApi) {

        var service = {
            create: create,
            createByDevice: createByDevice,
            createByFolder: createByFolder,
            createByTags: createByTags,
            list: list,
            listByDevice: listByDevice,
            remove: remove,
            update: update,
            info: info,
            schedule:schedule,
            updateSchedule: updateSchedule,
            removeSchedule: removeSchedule,
            link: link,
            unLink: unLink,
            enableRandom: enableRandom,
            disableRandom: disableRandom
        };

        return service;

        ////////////////
        function create(data) {
            return odApi.request('/0/Playlist/New', data);
        }

        function createByDevice(data) {
            return odApi.request('/0/Playlist/NewByDevice', data);
        }

        function createByFolder(data) {
            return odApi.request('/0/Playlist/NewByFolder', data);
        }

        function createByTags(data) {
            return odApi.request('/0/Playlist/NewByTags', data);
        }

        function list(data) {
            return odApi.request('/0/Playlist/List', data);
        }

        function listByDevice(data) {
            return odApi.request('/0/Playlist/ListByDevice', data);
        }

        function remove(data) {
            return odApi.request('/0/Playlist/Remove', data);
        }

        function update(data) {
            return odApi.request('/0/Playlist/Update', data);
        }

        function info(data) {
            return odApi.request('/0/Playlist/Info', data);
        }
        
        function schedule(data) {
            return odApi.request('/0/Playlist/Schedule', data);
        }
        
        function updateSchedule(data) {
            return odApi.request('/0/Playlist/UpdateSchedule', data);
        }
        
        function removeSchedule(data) {
            return odApi.request('/0/Playlist/RemoveSchedule', data);
        }

        function link(data) {
            return odApi.request('/0/Playlist/Link', data);
        }

        function unLink(data) {
            return odApi.request('/0/Playlist/UnLink', data);
        }

        function enableRandom(data) {
            return odApi.request('/0/Playlist/EnableRandom', data);
        }

        function disableRandom(data) {
            return odApi.request('/0/Playlist/DisableRandom', data);
        }
    }
})();
