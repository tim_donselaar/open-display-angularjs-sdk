(function() {
    'use strict';

    filesFactory.$inject = ["odApi"];
    angular
        .module('od.api')
        .factory('odFiles', filesFactory);

    /**
     * @ngdoc service 
     * @name od.api.odFiles
     * @description
     *
     * The `odFiles` service provides a convenient wrapper for file related requests.
     *
     */

    /* @ngInject */
    function filesFactory(odApi) {

        var service = {
            list: list,
            remove: remove,
            update: update,
            uploadURL: uploadURL,
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name list
         * @methodOf od.api.odFiles
         *
         * @description
         * With this function you can list all of the files.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Files/List
         * ```
         *
         * @param {Object=} data data object with the following properties.
         * - **ResultsStart** -[default=0] The starting point for the results.
         * - **ResultsLimit** - [default=25] The results limit.
         * - **Filter** - {Object} Filter object for sql where clause.
         *   - **Key** - ID, Name, Width, Height,Type, Status, TimeStamp.
         *   - **Selector** -  and, or. ***[default = and]***
         *   - **Operator** - like, not like, =, !=, <, >. ***[default = =]*** 
         *   - **Sort** -  ASC, DESC. ***[default = DESC]***
         *   - **Value** - Value for where clause.
         * 
         */
        function list(data) {
            return odApi.request('/0/Files/List', data);
        }

        /**
         * @ngdoc method
         * @name remove
         * @methodOf od.api.odFiles
         *
         * @description
         * With this function you can remove a file.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Files/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function remove(data) {
            return odApi.request('/0/Files/Remove', data);
        }

        /**
         * @ngdoc method
         * @name update
         * @methodOf od.api.odFiles
         *
         * @description
         * With this function you can update a existing file.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Files/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function update(data) {
            return odApi.request('/0/Files/Update', data);
        }

        /**
         * @ngdoc method
         * @name uploadURL
         * @methodOf od.api.odFiles
         *
         * @description
         * This function will give you a upload URL and UI URL.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Files/UploadURL
         * ```
         *
         */
        function uploadURL(data) {
            return odApi.request('/0/Files/UploadURL', data);
        }
    }
})();
