(function() {
    'use strict';

    deviceFactory.$inject = ["odApi"];
    angular
        .module('od.api')
        .factory('odDevice', deviceFactory);
    
    /**
     * @ngdoc service 
     * @name od.api.odDevice
     * @description
     *
     * The `odDevice` service provides a convenient wrapper to interact with the existing devices.
     *
     */

    /* @ngInject */
    function deviceFactory(odApi) {

        var service = {
            create: create,
            globalSettings: globalSettings,
            info: info,
            list: list,
            rebootReset: rebootReset,
            remove: remove,
            setFolder: setFolder,
            update: update,
            updateGlobalSettings: updateGlobalSettings
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name create
         * @methodOf od.api.odDevice
         *
         * @description
         * With this function you can create a new device.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Devices/New
         * ```
         *  
         * @param {Object} data data object with the following properties.
         *
         * - **Name** - `{string}` - The name of new device.
         * - **Key** - `{string}` - The unique key of the device that you want to register.
         *   This key will be displayed on screen of your device.
         * - **Tags** - `{array}` - An array of tags that will be attached to the device. 
         *   You can use tags to organize devices and easily publish/add playlist on tag bases.
         * - **Tree** - `{number}` - [default=0] The tree id where the device will be attached.

         * @returns {Promise} Promise that will be resolved when the request is fired. The value this
         *   promise will be resolved with is the return value of the `fn` function.
         */
        function create(data) {
            return odApi.request('/0/Devices/New', data);
        }

        /**
         * @ngdoc method
         * @name globalSettings
         * @methodOf od.api.odDevice
         *
         * @description
         * This function will retrieve the global settings.
         *
         * @param {Object} token session token retrieved from the Open Display Api.
         */
        function globalSettings(data) {
            return odApi.request('/0/Devices/GlobalSettings', data);
        }

        /**
         * @ngdoc method
         * @name info
         * @methodOf od.api.odDevice
         *
         * @description
         * This function will retrieve the information about the device.
         *
         * @param {Object} token session token retrieved from the Open Display Api.
         */
        function info(data) {
            return odApi.request('/0/Devices/Info', data);
        }

        /**
         * @ngdoc method
         * @name list
         * @methodOf od.api.odDevice
         *
         * @description
         * This function will list all devices you have access to.
         *
         * @param {Object} token session token retrieved from the Open Display Api.
         */
        function list(data) {
            return odApi.request('/0/Devices/List', data);
        }

        /**
         * @ngdoc method
         * @name rebootReset
         * @methodOf od.api.odDevice
         *
         * @description
         * You can use this function to reboot or reset the device.
         *
         * @param {Object} token session token retrieved from the Open Display Api.
         */
        function rebootReset(data) {
            return odApi.request('/0/Devices/RebootReset', data);
        }

        /**
         * @ngdoc method
         * @name remove
         * @methodOf od.api.odDevice
         *
         * @description
         * This function allows you to remove a device.
         *
         * @param {Object} token session token retrieved from the Open Display Api.
         */
        function remove(data) {
            return odApi.request('/0/Devices/Remove', data);
        }

        /**
         * @ngdoc method
         * @name setFolder
         * @methodOf od.api.odDevice
         *
         * @description
         * With this function you will be able to move the device to a folder.
         *
         * @param {Object} token session token retrieved from the Open Display Api.
         */
        function setFolder(data) {
            return odApi.request('/0/Devices/SetFolder', data);
        }

        /**
         * @ngdoc method
         * @name update
         * @methodOf od.api.odDevice
         *
         * @description
         * This function will allow you to update the device info and settings. DeviceVolume will overrule the global settings.
         *
         * @param {Object} token session token retrieved from the Open Display Api.
         */
        function update(data) {
            return odApi.request('/0/Devices/Update', data);
        }

        /**
         * @ngdoc method
         * @name updateGlobalSettings
         * @methodOf od.api.odDevice
         *
         * @description
         * This function will update the global device settings.
         *
         * @param {Object} token session token retrieved from the Open Display Api.
         */
        function updateGlobalSettings(data) {
            return odApi.request('/0/Devices/UpdateGlobalSettings', data);
        }

    }
})();
