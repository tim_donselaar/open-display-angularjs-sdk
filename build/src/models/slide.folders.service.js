(function() {
    'use strict';

    slideFoldersFactory.$inject = ["odApi"];
    angular
        .module('od.api')
        .factory('odSlideFolders', slideFoldersFactory);

    /**
     * @ngdoc service 
     * @name od.api.odSlideFolders
     * @description
     *
     * The `odSlideFolders` service provides a convenient wrapper for slideFolders related requests.
     *
     */

    /* @ngInject */
    function slideFoldersFactory(odApi) {

        var service = {
            create: create,
            list: list,
            remove: remove,
            update: update
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name create
         * @methodOf od.api.odSlideFolders
         *
         * @description
         * This function will allow you to create a folder for your slides.
         *
         * ```js
         * https://api.open-display.io/webapi/core/SlideFolders/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function create(data) {
            return odApi.request('/0/SlideFolders/New', data);
        }

        /**
         * @ngdoc method
         * @name list
         * @methodOf od.api.odSlideFolders
         *
         * @description
         * This function will list the folders.
         *
         * ```js
         * https://api.open-display.io/webapi/core/SlideFolders/List
         * ```
         *
         * @param {Object} data data options object.
         */
        function list(data) {
            return odApi.request('/0/SlideFolders/List', data);
        }

        /**
         * @ngdoc method
         * @name remove
         * @methodOf od.api.odSlideFolders
         *
         * @description
         * This function will allow you to remove a folder.
         *
         * ```js
         * https://api.open-display.io/webapi/core/SlideFolders/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function remove(data) {
            return odApi.request('/0/SlideFolders/Remove', data);
        }

        /**
         * @ngdoc method
         * @name update
         * @methodOf od.api.odSlideFolders
         *
         * @description
         * This function allows you to remove the folder.
         *
         * ```js
         * https://api.open-display.io/webapi/core/SlideFolders/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function update(data) {
            return odApi.request('/0/SlideFolders/Update', data);
        }
    }
})();
