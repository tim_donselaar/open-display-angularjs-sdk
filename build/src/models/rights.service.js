(function() {
    'use strict';

    rightsFactory.$inject = ["odApi"];
    angular
        .module('od.api')
        .factory('odRights', rightsFactory);

    /**
     * @ngdoc service 
     * @name od.api.odRights
     * @description
     *
     * The `odRights` service provides a convenient wrapper for rights related requests.
     *
     */

    /* @ngInject */
    function rightsFactory(odApi) {

        var service = {
            list: list,
            update: update,
            values: values
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name list
         * @methodOf od.api.odRights
         *
         * @description
         * 
         *
         * @param {Object} data data options object.
         */
        function list(data) {
            return odApi.request('/0/Rights/List', data);
        }

        /**
         * @ngdoc method
         * @name update
         * @methodOf od.api.odRights
         *
         * @description
         * 
         *
         * @param {Object} data data options object.
         */
        function update(data) {
            return odApi.request('/0/Rights/Save', data);
        }

        /**
         * @ngdoc method
         * @name values
         * @methodOf od.api.odRights
         *
         * @description
         * 
         *
         * @param {Object} data data options object.
         */
        function values(data) {
            return odApi.request('/0/Rights/Values', data);
        }
    }
})();
