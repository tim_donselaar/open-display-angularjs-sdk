(function() {
    'use strict';

    wadsFactory.$inject = ["odApi"];
    angular
        .module('od.api')
        .factory('odWads', wadsFactory);

    /**
     * @ngdoc service 
     * @name od.api.odWads
     * @description
     *
     * The `odWads` service provides a convenient wrapper for wads related requests.
     *
     */

    /* @ngInject */
    function wadsFactory(odApi) {

        var service = {
            list: list,
            remove: remove,
            update: update
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name list
         * @methodOf od.api.odWads
         *
         * @description
         * 
         *
         * @param {Object} data data options object.
         */
        function list(data) {
            return odApi.request('/5/WADS/List', data);
        }

        /**
         * @ngdoc method
         * @name remove
         * @methodOf od.api.odWads
         *
         * @description
         * 
         *
         * @param {Object} data data options object.
         */
        function remove(data) {
            return odApi.request('/5/WADS/Remove', data);
        }

        /**
         * @ngdoc method
         * @name update
         * @methodOf od.api.odWads
         *
         * @description
         * 
         *
         * @param {Object} data data options object.
         */
        function update(data) {
            return odApi.request('/5/WADS/Update', data);
        }
    }
})();
