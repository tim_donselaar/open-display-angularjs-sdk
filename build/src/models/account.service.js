(function() {
    'use strict';

    accountFactory.$inject = ["odApi"];
    angular
        .module('od.api')
        .factory('odAccount', accountFactory);

    /**
     * @ngdoc service 
     * @name od.api.odAccount
     * @description
     *
     * The `odAccount` service provides a convenient wrapper for account related requests.
     *
     */

    /* @ngInject */
    function accountFactory(odApi) {

        var service = {
            info: info,
            list: list,
            globalSettings: globalSettings,
            updateGlobalSettings: updateGlobalSettings,
            settings: settings,
            update: update,
            updatePassword: updatePassword,
            updateSettings: updateSettings
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name info
         * @methodOf od.api.odAccount
         *
         * @description
         * This function will return the account information, stats and branding.
         *
         * @param {Object} data data options object.
         */
        function info(data) {
            return odApi.request('/0/Account/Info', data);
        }

        /**
         * @ngdoc method
         * @name list
         * @methodOf od.api.odAccount
         *
         * @description
         * 
         *
         * ```js
         * https://api.open-display.io/webapi/core/Account/List
         * ```
         *
         * @param {Object} data data options object.
         */
        function list(data) {
            return odApi.request('/0/Account/List', data);
        }

        /**
         * @ngdoc method
         * @name globalSettings
         * @methodOf od.api.odAccount
         *
         * @description
         * This function allows you to retrieve the stored data that you have put inside a class.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Account/GetGlobalSettings
         * ```
         *
         * @param {Object} data data options object.
         */
        function globalSettings(data) {
            return odApi.request('/0/Account/GetGlobalSettings', data);
        }

        /**
         * @ngdoc method
         * @name updateGlobalSettings
         * @methodOf od.api.odAccount
         *
         * @description
         * With this function you can update the profile information of the user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Account/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function updateGlobalSettings(data) {
            return odApi.request('/0/Account/SaveGlobalSettings', data);
        }

         /**
         * @ngdoc method
         * @name settings
         * @methodOf od.api.odAccount
         *
         * @description
         * This function allows you to retrieve the stored data that you have put inside a class.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Account/Settings
         * ```
         *
         * @param {Object} data data options object.
         */
        function settings(data) {
            return odApi.request('/0/Account/Settings', data);
        }

        /**
         * @ngdoc method
         * @name update
         * @methodOf od.api.odAccount
         *
         * @description
         * With this function you can update the profile information of the user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Account/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function update(data) {
            return odApi.request('/0/Account/Update', data);
        }

        /**
         * @ngdoc method
         * @name updatePassword
         * @methodOf od.api.odAccount
         *
         * @description
         * This function allows you to update the password of the user. OldPassowrd is the current password that the user is using. 
         * Password is the new password and PasswordCheck is the validation of a password match.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Account/UpdatePassword
         * ```
         *
         * @param {Object} data data options object.
         */
        function updatePassword(data) {
            return odApi.request('/0/Account/UpdatePassword', data);
        }

        /**
         * @ngdoc method
         * @name updateSettings
         * @methodOf od.api.odAccount
         *
         * @description
         * We allow a flexible way of storing custom data to a profile the developer can specify a class and store an object in to this class. 
         * The class and data structure is up to the developer to decide.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Account/UpdateSettings
         * ```
         *
         * @param {Object} data data options object.
         *
         * 
         * ```
         * angular.module('updateAccountExample', ['od.account'])
         *     .controller('ExampleController', ['odAccount', function(odAccount) {
         *         var data = {
         *             Class: "Main",
         *             Data: {}
         *         };
         * 
         *         // Update custom settings
         *         odAccount.updateSettings(data).then(function(response) {
         *             console.log('settings are succesfully updated!');
         *         });
         *     }]);
         *
         * // Request object
         * RequestData: {
         *     Class: "Main",
         *     Data: {},
         *     SubAccountID: 0
         * };
         * 
         * 
         * // Response object
         * Response: {
         *     ApiInfo: {
         *         ApiVersion: 2,
         *         ServerTime: 1430387863
         *     },
         *     Session: {
         *         Token: "",
         *         UserType: "User",
         *         SubAccountID: 0,
         *         AccountID: 60,
         *         UserName: "useremail@domain.com"
         *     },
         *     ResponseHead: {
         *         Code: 200,
         *         Message: "OK"
         *     },
         *     ResponseBody: []
         * };
         * ```
         *
         */
        function updateSettings(data) {
            return odApi.request('/0/Account/UpdateSettings', data);
        }
    }
})();
