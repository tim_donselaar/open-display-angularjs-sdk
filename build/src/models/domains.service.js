(function() {
    'use strict';

    domainsFactory.$inject = ["odApi"];
    angular
        .module('od.api')
        .factory('odDomains', domainsFactory);

    /**
     * @ngdoc service 
     * @name od.api.odDomains
     * @description
     *
     * The `odDomains` service provides a convenient wrapper for domain related requests.
     *
     */

    /* @ngInject */
    function domainsFactory(odApi) {

        var service = {
            addUserToDomain: addUserToDomain,
            create: create,
            list: list,
            remove: remove,
            removeUserFromDomain: removeUserFromDomain
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name addUserToDomain
         * @methodOf od.api.odDomains
         *
         * @description
         * This function will allow you to add a user to the specified domain.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Domains/AddUserToDomain
         * ```
         *
         * @param {Object} data data options object.
         */
        function addUserToDomain(data) {
            return odApi.request('/0/Domains/AddUserToDomain', data);
        }

        /**
         * @ngdoc method
         * @name create
         * @methodOf od.api.odDomains
         *
         * @description
         * This function will allow you to create a domain. A domain is used for authenticating a sub user and for grouping. 
         * Domains are also global meaning that only one can exist system wide.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Domains/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function create(data) {
            return odApi.request('/0/Domains/New', data);
        }

        /**
         * @ngdoc method
         * @name list
         * @methodOf od.api.odDomains
         *
         * @description
         * This function will list all domains.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Domains/List
         * ```
         *
         * @param {Object} data data options object.
         */
        function list(data) {
            return odApi.request('/0/Domains/List', data);
        }

        /**
         * @ngdoc method
         * @name remove
         * @methodOf od.api.odDomains
         *
         * @description
         * This function will remove the domain.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Domains/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function remove(data) {
            return odApi.request('/0/Domains/Remove', data);
        }

        /**
         * @ngdoc method
         * @name removeUserFromDomain
         * @methodOf od.api.odDomains
         *
         * @description
         * This function will allow you to remove the user for the specified domain.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Domains/RemoveUserFromDomain
         * ```
         *
         * @param {Object} data data options object.
         */
        function removeUserFromDomain(data) {
            return odApi.request('/0/Domains/RemoveUserFromDomain', data);
        }
    }
})();
