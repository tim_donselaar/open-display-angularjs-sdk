(function() {
    'use strict';

    tagFactory.$inject = ["odApi"];
    angular
        .module('od.api')
        .factory('odTag', tagFactory);

    /**
     * @ngdoc service 
     * @name od.api.odTag
     * @description
     *
     * The `odTag` service provides a convenient wrapper for tag related requests.
     *
     */

    /* @ngInject */
    function tagFactory(odApi) {

        var service = {
            create: create,
            list: list,
            update: update,
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name create
         * @methodOf od.api.odTag
         *
         * @description
         * This function will allow you to create a new tag if the tag already exists it will also pass back api code 200.
         *
         * @param {Object} data data options object.
         */
        function create(data) {
            return odApi.request('/0/Tag/New', data);
        }

        /**
         * @ngdoc method
         * @name list
         * @methodOf od.api.odTag
         *
         * @description
         * This function will list all tags in your account.
         *
         * @param {Object} data data options object.
         */
        function list(data) {
            return odApi.request('/0/Tag/List', data);
        }

        /**
         * @ngdoc method
         * @name update
         * @methodOf od.api.odTag
         *
         * @description
         * This function will allow you to update a tag name.
         *
         * @param {Object} data data options object.
         */
        function update(data) {
            return odApi.request('/0/Tag/Update', data);
        }
    }
})();
