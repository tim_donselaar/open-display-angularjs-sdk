(function() {
    'use strict';

    ApiService.$inject = ["$http", "odConfig", "$cookies", "logger", "$rootScope", "$location", "$q", "odRetry"];
    APIRetry.$inject = ["$injector", "$q", "$timeout", "odConfig", "odRetry"];
    angular
        .module('od.api')
        .constant('odConfig', {
            APP_ID: '',
            AUTH_URL: 'https://api.open-display.io/login',
            API_URL: 'https://api.open-display.io/webapi',
            AUTO_REDIRECT: true,
            RETRYTIMEOUT: 2000,
            MAXRETRYS:5,
            MAXRETRYSBEFOREALERT: 2
        })
        .service('odApi', ApiService)
        .service('odRetry', RetryService)
        .factory('httpResponseErrorInterceptor', APIRetry)
        .config(["$httpProvider", function($httpProvider) {  
            $httpProvider.interceptors.push('httpResponseErrorInterceptor');
        }]);

    /* connection try 
        if the connection fails we try again in 5 seconds
    */
     function APIRetry($injector, $q, $timeout, odConfig, odRetry) {
        return {
            'responseError': function(response) {
            if (response.status !== 200) {
                return $timeout(function() {
                    if(odRetry.retryCounter === odConfig.MAXRETRYSBEFOREALERT){
                        odRetry.connectionErrorCallback();
                    }

                    if(odRetry.retryCounter >= odConfig.MAXRETRYS){
                        odRetry.unrecoverableError();
                    }

                    odRetry.retryCounter++;
                    var $http = $injector.get('$http');
                    return $http(response.config);
                }, odConfig.RETRYTIMEOUT);
            }
            return $q.reject(response);
            }
        };
    }

    /* retry service */
    function RetryService(){
        // init api service vars
        var openDisplayretry = this;

        openDisplayretry.retryCounter = 0;
        openDisplayretry.errorID = 0;

        openDisplayretry.connectionErrorCallback = function(){};
        openDisplayretry.connectionErrorRestoredCallback = function(){};
        openDisplayretry.unrecoverableError = function(){};
    }

    /* @ngInject */
    function ApiService($http, odConfig, $cookies, logger, $rootScope, $location, $q, odRetry) {
        // init api service vars
        var openDisplay = this;

        openDisplay.auth = false;
        openDisplay.token = $cookies.get('odToken');
        openDisplay.requestID = 0;

        // link functions
        openDisplay.setToken = setToken;
        openDisplay.login = login;
        openDisplay.request = request;
        openDisplay.logout = logout;

        activate();

        function activate() {
            // lets validate that the request is from the valide domains and set the token
            var TempToken = location.href.substr(location.href.length - 128);
            if (TempToken.length === 128) {
                openDisplay.setToken(TempToken);

                var stringLocation = location.href;
                stringLocation = stringLocation.replace('#' + TempToken, '');
                stringLocation = stringLocation.replace('#/' + TempToken, '');
                stringLocation = stringLocation.replace(TempToken, '');
                window.location.href = stringLocation;
            }
        }
        

        //////////////////////////////

        // set the session token and add a cookie
        function setToken(token) {
            $cookies.put('odToken', token);
            openDisplay.token = token;
        }

        // redirect to the login page
        function login() {
            var URL = '';

            if (odConfig.APP_ID !== '') {
                URL = odConfig.AUTH_URL + '?AppID=' + odConfig.APP_ID;
            } else {
                URL = odConfig.AUTH_URL + '?RedirectURL=' + encodeURIComponent($location.absUrl() + '#');
            }
            if (odConfig.AUTO_REDIRECT) {
                window.location.href = URL;
            }
        }

        // lets make a api request
        function request(url, data) {
            var deferred = $q.defer();
            
            $rootScope.$broadcast('event:openDisplayRequest', 'start');
            openDisplay.requestID++;
            $http({
                method: 'POST',
                url: odConfig.API_URL + url,
                transformRequest: formatData,
                data: {
                    Token: openDisplay.token,
                    RequestData: data ? angular.toJson(data) : angular.toJson({}),
                    REQUESTID: openDisplay.requestID
                },
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            }).then(done).catch(error);

            // on request done we handel the request
            function done(response) {
                $rootScope.$broadcast('event:openDisplayRequest', 'end');
                //restore connection if there is an error
                if(odRetry.retryCounter > 0){
                    odRetry.connectionErrorRestoredCallback();
                }
                odRetry.retryCounter = 0;
                if (response.data.ResponseHead.Code !== 401) {
                    // lets send the event that we are logged in
                    if (!openDisplay.auth) {
                        $rootScope.$broadcast('event:openDisplayAuth', true);
                    }

                    openDisplay.auth = true;

                    if (response.data.ResponseHead.Code !== 200) {
                        deferred.reject(response.data);
                        logger.apiError(response.data.ResponseHead.Code, response.data.ResponseHead.Message, response.data);
                    } else {
                        deferred.resolve(response.data);
                    }
                } else {
                    openDisplay.auth = false;
                    openDisplay.setToken('');
                    openDisplay.login();
                }
            }

            // if there is a connectin error we handel the message
            function error(response) {
                $rootScope.$broadcast('event:openDisplayRequest', 'end');
                logger.error(response);
            }

            // format the request to the server
            function formatData(obj) {
                var str = [];
                for (var p in obj) {
                    if (obj.hasOwnProperty(p)) {
                        str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
                    }
                }
                return str.join('&');
            }

            return deferred.promise;
        }

        // clear cookie and redirect tot the login page
        function logout() {
            openDisplay.setToken('');
            openDisplay.login();
        }
    }
})();
