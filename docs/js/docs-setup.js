NG_DOCS={
  "sections": {
    "api": "API Documentation"
  },
  "pages": [
    {
      "section": "api",
      "id": "od.api.odAccount",
      "shortName": "od.api.odAccount",
      "type": "service",
      "moduleName": "od.api",
      "shortDescription": "The odAccount service provides a convenient wrapper for account related requests.",
      "keywords": "account accountid allow allows angular api apiinfo apiversion branding class code console controller convenient current custom data decide developer examplecontroller flexible function globalsettings https info inside io js list log main match message method module object od odaccount oldpassowrd open-display options password passwordcheck profile request requestdata requests response responsebody responsehead retrieve return servertime service session settings stats store stored storing structure subaccountid succesfully token update updateaccountexample updated updateglobalsettings updatepassword updatesettings user useremail username usertype validation var wrapper"
    },
    {
      "section": "api",
      "id": "od.api.odApplication",
      "shortName": "od.api.odApplication",
      "type": "service",
      "moduleName": "od.api",
      "shortDescription": "The odApplication service provides a convenient wrapper for app related requests.",
      "keywords": "access accesslevels account allow allowed anytime api app application applications categories control convenient create custom data default devlist function grant group groups https io js list located method object od odapplication open-display options remove requests revokeaccess revoked rules service setgroup settings subuser subusers system update user users wrapper"
    },
    {
      "section": "api",
      "id": "od.api.odAuthenticate",
      "shortName": "od.api.odAuthenticate",
      "type": "service",
      "moduleName": "od.api",
      "shortDescription": "The odAuthenticate service provides a convenient wrapper for authentication.",
      "keywords": "allows api appid authentication behalf called check client convenient data destroyed function https io js key login logout method object od odauthenticate open-display options private receive request response send server service side token valid wrapper"
    },
    {
      "section": "api",
      "id": "od.api.odBilling",
      "shortName": "od.api.odBilling",
      "type": "service",
      "moduleName": "od.api",
      "shortDescription": "The odBilling service provides a convenient wrapper for account related requests.",
      "keywords": "account api branding convenient data function info method object od odbilling options requests return service stats wrapper"
    },
    {
      "section": "api",
      "id": "od.api.odCampaign",
      "shortName": "od.api.odCampaign",
      "type": "service",
      "moduleName": "od.api",
      "shortDescription": "The odCampaign service provides a convenient wrapper for campaign related requests.",
      "keywords": "api campaign clearresults convenient copy create data list method object od odcampaign options remove requests service update wrapper"
    },
    {
      "section": "api",
      "id": "od.api.odClients",
      "shortName": "od.api.odClients",
      "type": "service",
      "moduleName": "od.api",
      "shortDescription": "The odClients service provides a convenient wrapper for subusers related requests.",
      "keywords": "allow api convenient create data domain function getaccountinfo getaccountsettings getinvoiceurl group https info io js list method move object od odclients open-display options remove requests restore service subusers update updateextrafeeds updatelivehours updatetrial user users wrapper"
    },
    {
      "section": "api",
      "id": "od.api.odDevice",
      "shortName": "od.api.odDevice",
      "type": "service",
      "moduleName": "od.api",
      "shortDescription": "The odDevice service provides a convenient wrapper to interact with the existing devices.",
      "keywords": "access allow allows api array attached bases convenient create data device devices devicevolume display displayed easily existing fired fn folder function global globalsettings https info interact io js key list method move object od oddevice open open-display organize overrule playlist promise properties publish reboot rebootreset register remove request reset resolved retrieve retrieved return screen service session setfolder settings tag tags token tree unique update updateglobalsettings wrapper"
    },
    {
      "section": "api",
      "id": "od.api.odDeviceFolders",
      "shortName": "od.api.odDeviceFolders",
      "type": "service",
      "moduleName": "od.api",
      "shortDescription": "The odDeviceFolders service provides a convenient wrapper for deviceFolders related requests.",
      "keywords": "allow api convenient create data device devicefolders devices folder folders function https io js list method object od oddevicefolders open-display options parent remove requests service storing update wrapper"
    },
    {
      "section": "api",
      "id": "od.api.odDomains",
      "shortName": "od.api.odDomains",
      "type": "service",
      "moduleName": "od.api",
      "shortDescription": "The odDomains service provides a convenient wrapper for domain related requests.",
      "keywords": "add addusertodomain allow api authenticating convenient create data domain domains exist function global grouping https io js list meaning method object od oddomains open-display options remove removeuserfromdomain requests service system user wide wrapper"
    },
    {
      "section": "api",
      "id": "od.api.odFileFolders",
      "shortName": "od.api.odFileFolders",
      "type": "service",
      "moduleName": "od.api",
      "shortDescription": "The odFileFolders service provides a convenient wrapper for fileFolder related requests.",
      "keywords": "allow api convenient create data filefolder files folder folders function https io js list method object od odfilefolders open-display options parent remove requests service storing update wrapper"
    },
    {
      "section": "api",
      "id": "od.api.odFiles",
      "shortName": "od.api.odFiles",
      "type": "service",
      "moduleName": "od.api",
      "shortDescription": "The odFiles service provides a convenient wrapper for file related requests.",
      "keywords": "api asc clause convenient data desc existing file files filter function height https io js limit list method object od odfiles open-display options point properties remove requests service sql starting status timestamp type ui update upload uploadurl url width wrapper"
    },
    {
      "section": "api",
      "id": "od.api.odGroups",
      "shortName": "od.api.odGroups",
      "type": "service",
      "moduleName": "od.api",
      "shortDescription": "The odGroups service provides a convenient wrapper for group related requests.",
      "keywords": "access accessing add addusertogroup allow allows api convenient create data device devicefolder devices file filefolder files folder folders function functions group groups https ids io items js list method object od odgroups open-display options playlist playlists privileges read remove removeuserfromgroup requests restrict service set slide slidefolder slides system type types updateitem user wrapper write"
    },
    {
      "section": "api",
      "id": "od.api.odLog",
      "shortName": "od.api.odLog",
      "type": "service",
      "moduleName": "od.api",
      "shortDescription": "The odLog service provides a convenient wrapper for log related requests.",
      "keywords": "actions allow api convenient data function https io js list log login logins method object od odlog open-display options requests service wrapper"
    },
    {
      "section": "api",
      "id": "od.api.odPop",
      "shortName": "od.api.odPop",
      "type": "service",
      "moduleName": "od.api",
      "shortDescription": "The odPop service provides a convenient wrapper for proofOfPlay related requests.",
      "keywords": "add allow api campagins campaign convenient create data file function https impressions io js list method monitor object od odpop open-display options play proof proofofplay remove requests retrieve service settings stats update wrapper"
    },
    {
      "section": "api",
      "id": "od.api.odQuestions",
      "shortName": "od.api.odQuestions",
      "type": "service",
      "moduleName": "od.api",
      "shortDescription": "The odQuestions service provides a convenient wrapper for question related requests.",
      "keywords": "api convenient create data list method object od odquestions options question remove requests service update wrapper"
    },
    {
      "section": "api",
      "id": "od.api.odRights",
      "shortName": "od.api.odRights",
      "type": "service",
      "moduleName": "od.api",
      "shortDescription": "The odRights service provides a convenient wrapper for rights related requests.",
      "keywords": "api convenient data list method object od odrights options requests rights service update values wrapper"
    },
    {
      "section": "api",
      "id": "od.api.odSlide",
      "shortName": "od.api.odSlide",
      "type": "service",
      "moduleName": "od.api",
      "shortDescription": "The odSlide service provides a convenient wrapper for slide related requests.",
      "keywords": "access allow api approval cancel composite convenient create data directory function getsent html https import importsent io js list method object od odslide open-display options pending playform remove removesent request requests resolutions screen send service setdir slide slides supported update user wrapper"
    },
    {
      "section": "api",
      "id": "od.api.odSlideFolders",
      "shortName": "od.api.odSlideFolders",
      "type": "service",
      "moduleName": "od.api",
      "shortDescription": "The odSlideFolders service provides a convenient wrapper for slideFolders related requests.",
      "keywords": "allow allows api convenient create data folder folders function https io js list method object od odslidefolders open-display options remove requests service slidefolders slides update wrapper"
    },
    {
      "section": "api",
      "id": "od.api.odSubUsers",
      "shortName": "od.api.odSubUsers",
      "type": "service",
      "moduleName": "od.api",
      "shortDescription": "The odSubUsers service provides a convenient wrapper for subusers related requests.",
      "keywords": "allow api convenient create data domain function group https info io js list method object od odsubusers open-display options password remove requests retrieve service subusers update updatepassword user users wrapper"
    },
    {
      "section": "api",
      "id": "od.api.odTag",
      "shortName": "od.api.odTag",
      "type": "service",
      "moduleName": "od.api",
      "shortDescription": "The odTag service provides a convenient wrapper for tag related requests.",
      "keywords": "account allow api code convenient create data exists function list method object od odtag options pass requests service tag tags update wrapper"
    },
    {
      "section": "api",
      "id": "od.api.odWads",
      "shortName": "od.api.odWads",
      "type": "service",
      "moduleName": "od.api",
      "shortDescription": "The odWads service provides a convenient wrapper for wads related requests.",
      "keywords": "api convenient od odwads requests service wads wrapper"
    },
    {
      "section": "api",
      "id": "od.api.odWads",
      "shortName": "od.api.odWads",
      "type": "service",
      "moduleName": "od.api",
      "shortDescription": "The odWads service provides a convenient wrapper for wads related requests.",
      "keywords": "api convenient data list method object od odwads options remove requests service update wads wrapper"
    }
  ],
  "apis": {
    "api": true
  },
  "html5Mode": false,
  "editExample": true,
  "startPage": "/api",
  "scripts": [
    "angular.min.js"
  ]
};