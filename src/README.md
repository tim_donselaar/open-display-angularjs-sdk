# Open-Display Angular SDK #
_________________________________

Creator: Tim Donselaar

Version: 0.01

## Usage ##
________________________

```
#!javascript
// Add the od.api as a module dependency in your application 
var app = angular.module('sampleApp', ['od.api']);

app.controller('AppCtrl', ['$scope', 'odAccount', function($scope, odAccount) {
    odAccount.info().then(function(response) {
        // Lets show the users username in the view.
        $scope.myUsername = response.ResponseBody.Info.UserName;
    }, function(response) {
        // on error we log it in the console.
        console.log('Code: ' + response.ResponseHead.Code, ' Message:' + response.ResponseHead.Message);
    }); 
]);
```

## Functions ##
________________________

### odAccount ###
* info
* list
* settings
* update
* updatePassword
* updateSettings

### odApplication ###
* categories
* create
* devList
* list
* remove
* revokeAccess
* setGroup
* update

### odAuthenticate ###
* check
* login
* logout

### odCampaign ###
* copy
* create
* list
* remove
* update

### odDeviceFolders ###
* create
* list
* remove
* update

### odDevice ###
* create
* globalSettings
* info
* list
* rebootReset
* remove
* setNode
* update
* updateGlobalSettings

### odDomains ###
* addUserToDomain
* create
* list
* remove
* removeUserFromDomain

### odFeed ###
* create
* info
* list
* remove
* update

### odFileFolders ###
* create
* list
* remove
* update

### odFiles ###
* create
* download
* list
* remove
* uploadURL

### odGroups ###
* addUserToGroup
* create
* list
* remove
* removeUserFromGroup
* updateItem

### odLog ###
* list
* login

### odPlaylist ###
* create
* info
* link
* list
* listByDevice
* remove
* update
* unLink

### odPop ###
* create
* list
* stats
* remove
* update

### odWads ###
* create
* list
* remove
* update

### odRights ###
* list
* update
* values

### odSlideFolders ###
* create
* list
* remove
* update

### odSlide ###
* create
* get
* getSent
* importSent
* list
* remove
* removeSent
* resolutions
* send
* setDir
* update

### odSubUsers ###
* create
* info
* list
* remove
* update
* updatePassword

### odTag ###
* create
* list
* update

### odWads ###
* list
* remove
* update