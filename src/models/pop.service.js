(function() {
    'use strict';

    angular
        .module('od.api')
        .factory('odPop', popFactory);

    /**
     * @ngdoc service 
     * @name od.api.odPop
     * @description
     *
     * The `odPop` service provides a convenient wrapper for proofOfPlay related requests.
     *
     */

    /* @ngInject */
    function popFactory(odApi) {

        var service = {
            create: create,
            list: list,
            remove: remove,
            stats: stats,
            update: update
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name create
         * @methodOf od.api.odPop
         *
         * @description
         * This function will allow you to create a new proof of play campaign. You can add a file that you want to monitor for impressions.
         *
         * ```js
         * https://api.open-display.io/webapi/pop/Campaign/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function create(data) {
            return odApi.request('/4/Campaign/New', data);
        }

        /**
         * @ngdoc method
         * @name list
         * @methodOf od.api.odPop
         *
         * @description
         * This function will list all of the campagins.
         *
         * ```js
         * https://api.open-display.io/webapi/pop/Campaign/List
         * ```
         *
         * @param {Object} data data options object.
         */
        function list(data) {
            return odApi.request('/4/Campaign/List', data);
        }

        /**
         * @ngdoc method
         * @name remove
         * @methodOf od.api.odPop
         *
         * @description
         * This function will remove the campaign.
         *
         * ```js
         * https://api.open-display.io/webapi/pop/Campaign/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function remove(data) {
            return odApi.request('/4/Campaign/Remove', data);
        }

        /**
         * @ngdoc method
         * @name stats
         * @methodOf od.api.odPop
         *
         * @description
         * This function Will retrieve the stats of a proof of play campaign.
         *
         * ```js
         * https://api.open-display.io/webapi/pop/Campaign/Stats
         * ```
         *
         * @param {Object} data data options object.
         */
        function stats(data) {
            return odApi.request('/4/Campaign/Stats', data);
        }

        /**
         * @ngdoc method
         * @name update
         * @methodOf od.api.odPop
         *
         * @description
         * This function will allow you to update the settings of your proof of play campaign.
         *
         * ```js
         * https://api.open-display.io/webapi/pop/Campaign/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function update(data) {
            return odApi.request('/4/Campaign/Update', data);
        }
    }
})();
