(function() {
    'use strict';

    angular
        .module('od.api')
        .factory('odQuestions', questionsFactory);

    /**
     * @ngdoc service 
     * @name od.api.odQuestions
     * @description
     *
     * The `odQuestions` service provides a convenient wrapper for question related requests.
     *
     */

    /* @ngInject */
    function questionsFactory(odApi) {

        var service = {
            create: create,
            list: list,
            remove: remove,
            update: update
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name create
         * @methodOf od.api.odQuestions
         *
         * @description
         * 
         *
         * @param {Object} data data options object.
         */
        function create(data) {
            return odApi.request('/2/Questions/New', data);
        }

        /**
         * @ngdoc method
         * @name list
         * @methodOf od.api.odQuestions
         *
         * @description
         * 
         *
         * @param {Object} data data options object.
         */
        function list(data) {
            return odApi.request('/2/Questions/List', data);
        }

        /**
         * @ngdoc method
         * @name remove
         * @methodOf od.api.odQuestions
         *
         * @description
         * 
         *
         * @param {Object} data data options object.
         */
        function remove(data) {
            return odApi.request('/2/Questions/Remove', data);
        }

        /**
         * @ngdoc method
         * @name update
         * @methodOf od.api.odQuestions
         *
         * @description
         * 
         *
         * @param {Object} data data options object.
         */
        function update(data) {
            return odApi.request('/2/Questions/Update', data);
        }
    }
})();
