(function() {
    'use strict';

    angular
        .module('od.api')
        .factory('odBilling', billingFactory);

    /**
     * @ngdoc service 
     * @name od.api.odBilling
     * @description
     *
     * The `odBilling` service provides a convenient wrapper for account related requests.
     *
     */

    /* @ngInject */
    function billingFactory(odApi) {

        var service = {
            creditcardURL: creditcardURL,
            getCreditcard: getCreditcard,
            get: get,
            update: update,
            addCoupon: addCoupon,
            removeCoupon: removeCoupon,
            getInvoices: getInvoices,
            downloadInvoice: downloadInvoice,
            status:status,
            changePlan:changePlan,
            addAddon:addAddon,
            removeAddon: removeAddon
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name info
         * @methodOf od.api.odBilling
         *
         * @description
         * This function will return the account information, stats and branding.
         *
         * @param {Object} data data options object.
         */
        function creditcardURL(data) {
            return odApi.request('/0/Billing/CreditcardURL', data);
        }

         /**
         * @ngdoc method
         * @name info
         * @methodOf od.api.odBilling
         *
         * @description
         * This function will return the account information, stats and branding.
         *
         * @param {Object} data data options object.
         */
        function getCreditcard(data) {
            return odApi.request('/0/Billing/GetCreditcard', data);
        }

         /**
         * @ngdoc method
         * @name info
         * @methodOf od.api.odBilling
         *
         * @description
         * This function will return the account information, stats and branding.
         *
         * @param {Object} data data options object.
         */
        function get(data) {
            return odApi.request('/0/Billing/Get', data);
        }

         /**
         * @ngdoc method
         * @name info
         * @methodOf od.api.odBilling
         *
         * @description
         * This function will return the account information, stats and branding.
         *
         * @param {Object} data data options object.
         */
        function update(data) {
            return odApi.request('/0/Billing/Update', data);
        }

         /**
         * @ngdoc method
         * @name info
         * @methodOf od.api.odBilling
         *
         * @description
         * This function will return the account information, stats and branding.
         *
         * @param {Object} data data options object.
         */
        function addCoupon(data) {
            return odApi.request('/0/Billing/AddCoupon', data);
        }

         /**
         * @ngdoc method
         * @name info
         * @methodOf od.api.odBilling
         *
         * @description
         * This function will return the account information, stats and branding.
         *
         * @param {Object} data data options object.
         */
        function removeCoupon(data) {
            return odApi.request('/0/Billing/RemoveCoupon', data);
        }


        /**
         * @ngdoc method
         * @name info
         * @methodOf od.api.odBilling
         *
         * @description
         * This function will return the account information, stats and branding.
         *
         * @param {Object} data data options object.
         */
        function getInvoices(data) {
            return odApi.request('/0/Billing/GetInvoices', data);
        }

         /**
         * @ngdoc method
         * @name info
         * @methodOf od.api.odBilling
         *
         * @description
         * This function will return the account information, stats and branding.
         *
         * @param {Object} data data options object.
         */
        function downloadInvoice(data) {
            return odApi.request('/0/Billing/DownloadInvoice', data);
        }

        /**
         * @ngdoc method
         * @name info
         * @methodOf od.api.odBilling
         *
         * @description
         * This function will return the account information, stats and branding.
         *
         * @param {Object} data data options object.
         */
        function status(data) {
            return odApi.request('/0/Billing/Status', data);
        }

        /**
         * @ngdoc method
         * @name info
         * @methodOf od.api.odBilling
         *
         * @description
         * This function will return the account information, stats and branding.
         *
         * @param {Object} data data options object.
         */
        function changePlan(data) {
            return odApi.request('/0/Billing/ChangePlan', data);
        }

        /**
         * @ngdoc method
         * @name info
         * @methodOf od.api.odBilling
         *
         * @description
         * This function will return the account information, stats and branding.
         *
         * @param {Object} data data options object.
         */
        function addAddon(data) {
            return odApi.request('/0/Billing/AddAddon', data);
        }

        /**
         * @ngdoc method
         * @name info
         * @methodOf od.api.odBilling
         *
         * @description
         * This function will return the account information, stats and branding.
         *
         * @param {Object} data data options object.
         */
        function removeAddon(data) {
            return odApi.request('/0/Billing/RemoveAddon', data);
        }
    }
})();
