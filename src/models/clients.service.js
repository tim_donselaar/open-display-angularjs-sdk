(function() {
    'use strict';

    angular
        .module('od.api')
        .factory('odClients', ClientsFactory);

    /**
     * @ngdoc service 
     * @name od.api.odClients
     * @description
     *
     * The `odClients` service provides a convenient wrapper for subusers related requests.
     *
     */

    /* @ngInject */
    function ClientsFactory(odApi) {

        var service = {
            create: create,
            list: list,
            remove: remove,
            restore:restore,
            update: update,
            resetPassword:resetPassword,
            updateSettings: updateSettings,
            updateInfo: updateInfo,
            getAccountInfo: getAccountInfo,
            getAccountSettings: getAccountSettings,
            updateTrial: updateTrial,

            listDevices: listDevices,
            updateDevice: updateDevice,
            getDeviceInfo: getDeviceInfo,
            updatePlaylist:updatePlaylist,
            
            move: move,
            getInvoiceURL:getInvoiceURL,
            getLoginLog:getLoginLog,
            getActionLog:getActionLog
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name create
         * @methodOf od.api.odClients
         *
         * @description
         * This function will allow you create a sub user, you do need to create a domain and group beforehand.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function create(data) {
            return odApi.request('/1/Clients/New', data);
        }

        /**
         * @ngdoc method
         * @name list
         * @methodOf od.api.odClients
         *
         * @description
         * This function will list all sub users.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/List
         * ```
         *
         * @param {Object} data data options object.
         */
        function list(data) {
            return odApi.request('/1/Clients/List', data);
        }


        /**
         * @ngdoc method
         * @name remove
         * @methodOf od.api.odClients
         *
         * @description
         * This function will allow you to remove the sub user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function remove(data) {
            return odApi.request('/1/Clients/Remove', data);
        }

        /**
         * @ngdoc method
         * @name restore
         * @methodOf od.api.odClients
         *
         * @description
         * This function will allow you to restore the sub user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function restore(data) {
            return odApi.request('/1/Clients/Restore', data);
        }

        /**
         * @ngdoc method
         * @name update
         * @methodOf od.api.odClients
         *
         * @description
         * This function will update the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function update(data) {
            return odApi.request('/1/Clients/Update', data);
        }

         /**
         * @ngdoc method
         * @name update
         * @methodOf od.api.odClients
         *
         * @description
         * This function will update the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/resetPassword
         * ```
         *
         * @param {Object} data data options object.
         */
        function resetPassword(data) {
            return odApi.request('/1/Clients/ResetPassword', data);
        }

        /**
         * @ngdoc method
         * @name update
         * @methodOf od.api.odClients
         *
         * @description
         * This function will update the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/resetPassword
         * ```
         *
         * @param {Object} data data options object.
         */
        function updateSettings(data) {
            return odApi.request('/1/Clients/UpdateSettings', data);
        }

        /**
         * @ngdoc method
         * @name update
         * @methodOf od.api.odClients
         *
         * @description
         * This function will update the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/resetPassword
         * ```
         *
         * @param {Object} data data options object.
         */
        function updateInfo(data) {
            return odApi.request('/1/Clients/UpdateInfo', data);
        }

        /**
         * @ngdoc method
         * @name getAccountInfo
         * @methodOf od.api.odClients
         *
         * @description
         * This function will allow you to getAccountInfo the sub user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function getAccountInfo(data) {
            return odApi.request('/1/Clients/GetAccountInfo', data);
        }

        /**
         * @ngdoc method
         * @name getAccountSettings
         * @methodOf od.api.odClients
         *
         * @description
         * This function will allow you to getAccountSettings the sub user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function getAccountSettings(data) {
            return odApi.request('/1/Clients/GetAccountSettings', data);
        }

        /**
         * @ngdoc method
         * @name updateTrial
         * @methodOf od.api.odClients
         *
         * @description
         * This function will allow you to updateTrial the sub user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function updateTrial(data) {
            return odApi.request('/1/Clients/UpdateTrial', data);
        }

        /**
         * @ngdoc method
         * @name updateLiveHours
         * @methodOf od.api.odClients
         *
         * @description
         * This function will allow you to updateLiveHours the sub user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function updateLiveHours(data) {
            return odApi.request('/1/Clients/UpdateLiveHours', data);
        }

        /**
         * @ngdoc method
         * @name updateExtraFeeds
         * @methodOf od.api.odClients
         *
         * @description
         * This function will allow you to updateExtraFeeds the sub user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function updateExtraFeeds(data) {
            return odApi.request('/1/Clients/UpdateExtraFeeds', data);
        }

        /**
         * @ngdoc method
         * @name move
         * @methodOf od.api.odClients
         *
         * @description
         * This function will allow you to move the sub user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function move(data) {
            return odApi.request('/1/Clients/Move', data);
        }

        /**
         * @ngdoc method
         * @name getInvoiceURL
         * @methodOf od.api.odClients
         *
         * @description
         * This function will allow you to getInvoiceURL the sub user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function getInvoiceURL(data) {
            return odApi.request('/1/Clients/GetInvoiceURL', data);
        }

        /**
         * @ngdoc method
         * @name getInvoiceURL
         * @methodOf od.api.odClients
         *
         * @description
         * This function will allow you to getInvoiceURL the sub user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function getLoginLog(data) {
            return odApi.request('/1/Clients/GetLoginLog', data);
        }

        /**
         * @ngdoc method
         * @name getInvoiceURL
         * @methodOf od.api.odClients
         *
         * @description
         * This function will allow you to getInvoiceURL the sub user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function getActionLog(data) {
            return odApi.request('/1/Clients/GetActionLog', data);
        }
    }
})();
