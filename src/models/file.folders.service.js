(function() {
    'use strict';

    angular
        .module('od.api')
        .factory('odFileFolders', fileFoldersFactory);

    /**
     * @ngdoc service 
     * @name od.api.odFileFolders
     * @description
     *
     * The `odFileFolders` service provides a convenient wrapper for fileFolder related requests.
     *
     */

    /* @ngInject */
    function fileFoldersFactory(odApi) {

        var service = {
            create: create,
            list: list,
            remove: remove,
            update: update
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name create
         * @methodOf od.api.odFileFolders
         *
         * @description
         * This function will allow you to make a folder for your files.
         *
         * ```js
         * https://api.open-display.io/webapi/core/FilesFolders/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function create(data) {
            return odApi.request('/0/FilesFolders/New', data);
        }

        /**
         * @ngdoc method
         * @name list
         * @methodOf od.api.odFileFolders
         *
         * @description
         * This function will list all of the folders that are used for storing your files.
         *
         * ```js
         * https://api.open-display.io/webapi/core/FilesFolders/List
         * ```
         *
         * @param {Object} data data options object.
         */
        function list(data) {
            return odApi.request('/0/FilesFolders/List', data);
        }

        /**
         * @ngdoc method
         * @name remove
         * @methodOf od.api.odFileFolders
         *
         * @description
         * This function will allow you to remove the folder.
         *
         * ```js
         * https://api.open-display.io/webapi/core/FilesFolders/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function remove(data) {
            return odApi.request('/0/FilesFolders/Remove', data);
        }

        /**
         * @ngdoc method
         * @name update
         * @methodOf od.api.odFileFolders
         *
         * @description
         * This function will allow you to update the folder name and parent folder id.
         *
         * ```js
         * https://api.open-display.io/webapi/core/FilesFolders/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function update(data) {
            return odApi.request('/0/FilesFolders/Update', data);
        }
    }
})();
