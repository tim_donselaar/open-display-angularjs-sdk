(function() {
    'use strict';

    angular
        .module('od.api')
        .factory('odSlide', slideFactory);

    /**
     * @ngdoc service 
     * @name od.api.odSlide
     * @description
     *
     * The `odSlide` service provides a convenient wrapper for slide related requests.
     *
     */
    /* @ngInject */
    function slideFactory(odApi) {

        var service = {
            create: create,
            get: get,
            getSent: getSent,
            list: list,
            importSent: importSent,
            remove: remove,
            removeSent: removeSent,
            resolutions: resolutions,
            send: send,
            setDir: setDir,
            update: update
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name create
         * @methodOf od.api.odSlide
         *
         * @description
         * This function will allow you to create a slide. a slide is a html composite of the the screen.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Slide/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function create(data) {
            return odApi.request('/0/Slide/New', data);
        }

        /**
         * @ngdoc method
         * @name get
         * @methodOf od.api.odSlide
         *
         * @description
         * This function will allow you to request a slide.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Slide/Get
         * ```
         *
         * @param {Object} data data options object.
         */
        function get(data) {
            return odApi.request('/0/Slide/Get', data);
        }

        /**
         * @ngdoc method
         * @name getSent
         * @methodOf od.api.odSlide
         *
         * @description
         * This function will allow you to get a list of slide that are pending approval.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Slide/GetSentThemes
         * ```
         *
         * @param {Object} data data options object.
         */
        function getSent(data) {
            return odApi.request('/0/Slide/GetSentThemes', data);
        }

        /**
         * @ngdoc method
         * @name list
         * @methodOf od.api.odSlide
         *
         * @description
         * This function will list the slides.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Slide/List
         * ```
         *
         * @param {Object} data data options object.
         */
        function list(data) {
            return odApi.request('/0/Slide/List', data);
        }

        /**
         * @ngdoc method
         * @name importSent
         * @methodOf od.api.odSlide
         *
         * @description
         * This function will allow you to access the slide import request.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Slide/ImportSentTheme
         * ```
         *
         * @param {Object} data data options object.
         */
        function importSent(data) {
            return odApi.request('/0/Slide/ImportSentTheme', data);
        }

        /**
         * @ngdoc method
         * @name remove
         * @methodOf od.api.odSlide
         *
         * @description
         * This function will allow you to remove a slide.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Slide/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function remove(data) {
            return odApi.request('/0/Slide/Remove', data);
        }

        /**
         * @ngdoc method
         * @name removeSent
         * @methodOf od.api.odSlide
         *
         * @description
         * This function will allow you to cancel the slide import request.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Slide/RemoveSentTheme
         * ```
         *
         * @param {Object} data data options object.
         */
        function removeSent(data) {
            return odApi.request('/0/Slide/RemoveSentTheme', data);
        }

        /**
         * @ngdoc method
         * @name resolutions
         * @methodOf od.api.odSlide
         *
         * @description
         * This function will allow you to list supported resolutions.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Slide/Resolutions
         * ```
         *
         * @param {Object} data data options object.
         */
        function resolutions(data) {
            return odApi.request('/0/Slide/Resolutions', data);
        }

        /**
         * @ngdoc method
         * @name send
         * @methodOf od.api.odSlide
         *
         * @description
         * With this function you can send a slide to another user with in the open-display playform.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Slide/SendTheme
         * ```
         *
         * @param {Object} data data options object.
         */
        function send(data) {
            return odApi.request('/0/Slide/SendTheme', data);
        }

        /**
         * @ngdoc method
         * @name setDir
         * @methodOf od.api.odSlide
         *
         * @description
         * This function will allow you to put a slide in a directory.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Slide/SetDir
         * ```
         *
         * @param {Object} data data options object.
         */
        function setDir(data) {
            return odApi.request('/0/Slide/SetDir', data);
        }

        /**
         * @ngdoc method
         * @name update
         * @methodOf od.api.odSlide
         *
         * @description
         * This function will allow you to update a slide.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Slide/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function update(data) {
            return odApi.request('/0/Slide/Update', data);
        }
    }
})();
