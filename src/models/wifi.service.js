(function() {
    'use strict';

    angular
        .module('od.api')
        .factory('odWIFI', wifiFactory);

    /**
     * @ngdoc service 
     * @name od.api.odWads
     * @description
     *
     * The `odWads` service provides a convenient wrapper for wads related requests.
     *
     */

    /* @ngInject */
    function wifiFactory(odApi) {

        var service = {
            get: get
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name get
         * @methodOf od.api.odWads
         *
         * @description
         * 
         *
         * @param {Object} data data options object.
         */
        function get(data) {
            return odApi.request('/6/WIFI/Get', data);
        }
    }
})();
