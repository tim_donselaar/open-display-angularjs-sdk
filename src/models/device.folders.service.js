(function() {
    'use strict';

    angular
        .module('od.api')
        .factory('odDeviceFolders', deviceFoldersFactory);

    /**
     * @ngdoc service 
     * @name od.api.odDeviceFolders
     * @description
     *
     * The `odDeviceFolders` service provides a convenient wrapper for deviceFolders related requests.
     *
     */

    /* @ngInject */
    function deviceFoldersFactory(odApi) {

        var service = {
            create: create,
            list: list,
            remove: remove,
            update: update
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name create
         * @methodOf od.api.odDeviceFolders
         *
         * @description
         * With this function you can create a folder for storing your devices.
         *
         * ```js
         * https://api.open-display.io/webapi/core/DeviceFolders/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function create(data) {
            return odApi.request('/0/DeviceFolders/New', data);
        }

        /**
         * @ngdoc method
         * @name list
         * @methodOf od.api.odDeviceFolders
         *
         * @description
         * With this function you can list all of your device folders.
         *
         * ```js
         * https://api.open-display.io/webapi/core/DeviceFolders/List
         * ```
         *
         * @param {Object} data data options object.
         */
        function list(data) {
            return odApi.request('/0/DeviceFolders/List', data);
        }

        /**
         * @ngdoc method
         * @name remove
         * @methodOf od.api.odDeviceFolders
         *
         * @description
         * This function will allow you to remove the device folder.
         *
         * ```js
         * https://api.open-display.io/webapi/core/DeviceFolders/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function remove(data) {
            return odApi.request('/0/DeviceFolders/Remove', data);
        }

        /**
         * @ngdoc method
         * @name update
         * @methodOf od.api.odDeviceFolders
         *
         * @description
         * This function will allow you to update name and the parent folder.
         *
         * ```js
         * https://api.open-display.io/webapi/core/DeviceFolders/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function update(data) {
            return odApi.request('/0/DeviceFolders/Update', data);
        }
    }
})();
