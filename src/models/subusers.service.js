(function() {
    'use strict';

    angular
        .module('od.api')
        .factory('odSubUsers', subUsersFactory);

    /**
     * @ngdoc service 
     * @name od.api.odSubUsers
     * @description
     *
     * The `odSubUsers` service provides a convenient wrapper for subusers related requests.
     *
     */

    /* @ngInject */
    function subUsersFactory(odApi) {

        var service = {
            create: create,
            list: list,
            info: info,
            remove: remove,
            update: update,
            updatePassword: updatePassword
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name create
         * @methodOf od.api.odSubUsers
         *
         * @description
         * This function will allow you create a sub user, you do need to create a domain and group beforehand.
         *
         * ```js
         * https://api.open-display.io/webapi/core/SubUsers/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function create(data) {
            return odApi.request('/0/SubUsers/New', data);
        }

        /**
         * @ngdoc method
         * @name list
         * @methodOf od.api.odSubUsers
         *
         * @description
         * This function will list all sub users.
         *
         * ```js
         * https://api.open-display.io/webapi/core/SubUsers/List
         * ```
         *
         * @param {Object} data data options object.
         */
        function list(data) {
            return odApi.request('/0/SubUsers/List', data);
        }

        /**
         * @ngdoc method
         * @name info
         * @methodOf od.api.odSubUsers
         *
         * @description
         * This function will retrieve the sub users information.
         *
         * ```js
         * https://api.open-display.io/webapi/core/SubUsers/Info
         * ```
         *
         * @param {Object} data data options object.
         */
        function info(data) {
            return odApi.request('/0/SubUsers/Info', data);
        }

        /**
         * @ngdoc method
         * @name remove
         * @methodOf od.api.odSubUsers
         *
         * @description
         * This function will allow you to remove the sub user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/SubUsers/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function remove(data) {
            return odApi.request('/0/SubUsers/Remove', data);
        }

        /**
         * @ngdoc method
         * @name update
         * @methodOf od.api.odSubUsers
         *
         * @description
         * This function will update the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/SubUsers/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function update(data) {
            return odApi.request('/0/SubUsers/Update', data);
        }

        /**
         * @ngdoc method
         * @name updatePassword
         * @methodOf od.api.odSubUsers
         *
         * @description
         * This function will allow you to update the password of the user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/SubUsers/UpdatePassword
         * ```
         *
         * @param {Object} data data options object.
         */
        function updatePassword(data) {
            return odApi.request('/0/SubUsers/UpdatePassword', data);
        }
    }
})();
