(function () {
    'use strict';

    angular
            .module('od.api')
            .factory('odFeed', feedFactory);

    function feedFactory(odApi) {

        var service = {
            create: create,
            update: update,
            Remove: Remove,
            list: list,
            info: info
        };

        return service;

        ////////////////
        function create(data) {
            return odApi.request('/0/Feed/New', data);
        }
        
        function update(data) {
            return odApi.request('/0/Feed/Update', data);
        }
        
        function Remove(data) {
            return odApi.request('/0/Feed/Remove', data);
        }
        
        function list(data) {
            return odApi.request('/0/Feed/List', data);
        }
        
        function info(data) {
            return odApi.request('/0/Feed/Info', data);
        }
    }
})();