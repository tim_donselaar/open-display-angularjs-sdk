(function() {
    'use strict';

    angular
        .module('od.api')
        .factory('odAuthenticate', authenticateFactory);

    /**
     * @ngdoc service 
     * @name od.api.odAuthenticate
     * @description
     *
     * The `odAuthenticate` service provides a convenient wrapper for authentication.
     *
     */

    /* @ngInject */
    function authenticateFactory(odApi) {

        var service = {
            auth: auth,
            check: check,
            login: login,
            logout: logout
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name auth
         * @methodOf od.api.odAuthenticate
         *
         * @description
         * This function is used for server side authentication you will need to send the AppID and private key to the api. 
         * On server response you will receive the token. this token allows for you to make request on behalf of the client.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Authenticate/Auth
         * ```
         *
         * @param {Object} data data options object.
         */
        function auth(data) {
            return odApi.request('/0/Authenticate/Auth', data);
        }

        /**
         * @ngdoc method
         * @name check
         * @methodOf od.api.odAuthenticate
         *
         * @description
         * To check if the token is still valid you use this function.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Authenticate/check
         * ```
         *
         * @param {Object} data data options object.
         */
        function check() {
            return odApi.request('/0/Authenticate/check');
        }

        /**
         * @ngdoc method
         * @name login
         * @methodOf od.api.odAuthenticate
         *
         * @description
         * 
         * @param {Object} data data options object.
         */
        function login() {
            odApi.login();
        }

        /**
         * @ngdoc method
         * @name logout
         * @methodOf od.api.odAuthenticate
         *
         * @description
         * When this function is called the token is destroyed.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Authenticate/Logout
         * ```
         *
         * @param {Object} data data options object.
         */
        function logout() {
            return odApi.request('/0/Authenticate/Logout').then(function() {
                odApi.logout();
            });
        }
    }
})();
