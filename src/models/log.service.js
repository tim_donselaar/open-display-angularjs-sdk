(function() {
    'use strict';

    angular
        .module('od.api')
        .factory('odLog', logFactory);

    /**
     * @ngdoc service 
     * @name od.api.odLog
     * @description
     *
     * The `odLog` service provides a convenient wrapper for log related requests.
     *
     */

    /* @ngInject */
    function logFactory(odApi) {

        var service = {
            list: list,
            login: login
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name list
         * @methodOf od.api.odLog
         *
         * @description
         * This function will allow you to list all actions.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Log/List
         * ```
         *
         * @param {Object} data data options object.
         */
        function list(data) {
            return odApi.request('/0/Log/List', data);
        }

        /**
         * @ngdoc method
         * @name login
         * @methodOf od.api.odLog
         *
         * @description
         * This function will allow you to list the logins.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Log/Login
         * ```
         *
         * @param {Object} data data options object.
         */
        function login(data) {
            return odApi.request('/0/Log/Login', data);
        }
    }
})();
