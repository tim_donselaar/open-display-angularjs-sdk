(function() {
    'use strict';

    // init the Open-Display API module
    angular.module('od.api', [
        'ngCookies'
    ]);
})();