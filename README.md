# Open-Display Angular SDK #
_________________________________

## Introduction ##
________________________

Welcome to the Open Display API doc.

In this document we describe how to connect and run api functions. We currently have a angularjs sdk more sdk are in the work. 


## Authentication ##
________________________

We support two ways of authenticating, client side and server side authentication.
 
First step is to create a new application in your account dashboard.
 
#### Create an app ####
 
- Login to your account dashboard.
 
- Go to Apps -> Custom Apps -> plus button on the top left.
 
- Fill in all of the fields. note. the app URL is the URL that the user is redirected once logged in we will put the auth token at the end of this URL.
 
#### Client side authentication ####
 
Flow: Redirect user to the login page with the AppID or RedirectURL -> user is redirect back to the app with at the end of the url the auth token.
 
It is up to the client side application to get the auth token and store it locally on the client’s device. This way the application can use the auth token to make api request on behalf of the client. The token has a limited lifespan so when a api request fails due to that the user token is invalid (Return code: 401) restart the authentication flow.
 
#### Server side authentication ####
 
Flow: Auth using the private key -> success server returns a token that is valid x number of seconds -> use the token to do api calls on behalf of the app owner.
 
Once you have created an app you can press the edit icon of the app you will be shown the settings of the app and the private key.
 
The private key is needed to do server side logins as the owner of the account in which you have created the app.